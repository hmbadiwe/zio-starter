name := "zio-starter"

version := "0.1"

scalaVersion := "2.13.7"

val http4s = "0.23.6"
val doobie = "1.0.0-RC1"
val zioInteropCats = "3.1.1.0"
val pureConfig = "0.17.0"
val circe = "0.14.1"
libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % "1.0.12",
  "dev.zio" %% "zio-kafka" % "0.17.1",
  "dev.zio" %% "zio-json" % "0.1.5",
  "org.http4s" %% "http4s-blaze-server" % http4s,
  "org.http4s" %% "http4s-circe" % http4s,
  "org.http4s" %% "http4s-dsl" % http4s,
  "org.tpolecat" %% "doobie-core" % doobie,
  "org.tpolecat" %% "doobie-postgres" % doobie,
  "org.tpolecat" %% "doobie-h2" % doobie,
  "dev.zio" %% "zio-interop-cats" % zioInteropCats,
  "com.github.pureconfig" %% "pureconfig" % pureConfig,
  "io.circe" %% "circe-generic" % circe,
  "io.circe" %% "circe-generic-extras" % circe

)