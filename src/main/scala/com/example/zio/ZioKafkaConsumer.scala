package com.example.zio

import com.example.zio.db.DbConfiguration.DbConfigurationEnv
import com.example.zio.db.{DbConfiguration, GamePersistence, GamePersistenceService}
import com.example.zio.db.GamePersistence.{GamePersistenceEnv, retrieveAllGames}
import com.example.zio.db.GamePersistenceService.DbTransactorEnv
import com.example.zio.domain.Game
import com.example.zio.http.{ApiConfiguration, ApiRoute}
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.implicits._
import zio.interop.catz._
import org.http4s.server.Router
import zio.kafka.consumer.{CommittableRecord, Consumer, ConsumerSettings, Subscription}
import zio.kafka.serde.Serde
import zio.{ExitCode, Has, RIO, RManaged, URIO, ZEnv, ZIO, ZLayer, console}
import zio.json._
import zio.stream.{ZSink, ZStream}
import cats.effect.{ExitCode => CatsExitCode}
import com.example.zio.http.ApiConfiguration.ApiConfigurationEnv

import scala.concurrent.duration._
object ZioKafkaConsumer extends zio.App {

  val consumerSettings = ConsumerSettings(List("localhost:9092"))
    .withGroupId("updates-consumer")

  val managedConsumer: RManaged[zio.clock.Clock with zio.blocking.Blocking, Consumer] = Consumer.make(consumerSettings)

  val consumer = ZLayer.fromManaged(managedConsumer)

  val topicName = "updates"

  val gameSerde: Serde[Any, Game] = Serde.string.inmapM { jsonStr =>
    ZIO.fromEither(jsonStr.fromJson[Game].left.map(err => new RuntimeException(err)))
  }{ matchVal =>
    ZIO.effect(matchVal.toJson)
  }


  type AppEnvironment =
      Has[Consumer]
      with DbTransactorEnv
      with DbConfigurationEnv
      with ApiConfigurationEnv
      with GamePersistenceEnv
      with zio.console.Console
      with zio.clock.Clock
      with zio.blocking.Blocking
      with zio.system.System
      with zio.random.Random

  type AppTask[A] = RIO[AppEnvironment, A]

  val matchesStream: ZIO[AppEnvironment, Throwable, Unit] =
    Consumer
      .subscribeAnd(Subscription.topics(topicName))
      .plainStream(Serde.string, gameSerde) // stream of match instance
      .map(cr => (cr.value, cr.offset)) // stream of tuples (String, offset)
      .chunkN(1)
      .tap{ case (game, _) => zio.console.putStrLn(s"Game received: $game")}
      .tap{ case (game, _) => GamePersistence.createGame(game)}
      .throttleShapeM(1, zio.duration.Duration.fromScala(1.second))(_ => ZIO.succeed(1))
      .map(_._2) // keep the offsets - stream of offsets
      .aggregateAsync(Consumer.offsetBatches) // stream of offsets
      .run(ZSink.foreach(_.commit))


  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val dbTransactorLive = (DbConfiguration.live ++ zio.blocking.Blocking.live) >>> GamePersistenceService.transactorLive
    val gamePersistenceLive = dbTransactorLive >>> GamePersistence.live
    val live = consumer ++ zio.console.Console.live ++
      zio.clock.Clock.live ++
      zio.blocking.Blocking.live ++
      zio.system.System.live ++
      zio.random.Random.live ++
      DbConfiguration.live ++
      ApiConfiguration.live ++
      dbTransactorLive ++
      gamePersistenceLive


    val httpProgram: ZIO[AppEnvironment, Throwable, Unit] =
      for {
        api <- ApiConfiguration.loadApiConfig
        httpApp = Router[AppTask]("/games" -> ApiRoute().route).orNotFound
        server <- ZIO.runtime[AppEnvironment].flatMap{ implicit rts =>
          BlazeServerBuilder[AppTask]
            .bindHttp(api.port, api.endpoint)
            .withHttpApp(httpApp)
            .serve
            .compile[AppTask, AppTask, CatsExitCode]
            .drain
        }
      } yield server



    (httpProgram.provideSomeLayer(live) &> matchesStream.provideSomeLayer(live)).exitCode
  }
}
