package com.example.zio

import zio.{ExitCode, Has, Task, ULayer, URIO, ZIO, ZLayer}
import zio.console._
import zio.duration._
object ZioApp extends zio.App {
  case class User(name: String, emailAddress: String)

  type ZioServe[S] = ZIO[Has[S], Throwable, Unit]
  object UserEmailer {
    trait Service {
      def notify(user: User, message: String): Task[Unit]
    }

    type UserEmailerEnv = Has[Service]

    val liveCanon: ZLayer[Any, Nothing, UserEmailerEnv] = ZLayer.succeed(
      new Service {
        override def notify(user: User, message: String): Task[Unit] = Task {
          println(s"[User emailer] sending $message to ${user.emailAddress}")
        }
      }
    )

    val live: ULayer[UserEmailerEnv] = ZLayer.succeed(
      (user: User, message: String) => Task {
        println(s"[User emailer] sending $message to ${user.emailAddress}")
      }
    )

    def notify(user: User, message: String): ZioServe[Service] =
      ZIO.accessM(_.get.notify(user, message))
  }

  object UserDb {
    trait Service {
      def insert(user: User): Task[Unit]
      //def query(queryTmpl: String, params: Map[String, String]): Task[User]
    }
    type UserDbEnv = Has[Service]

    val live: ULayer[UserDbEnv] = ZLayer.succeed(
      (user: User) => Task{
        println(s"Inserting $user into database")
      }
    )
    def insert(user: User): ZioServe[Service] =
      ZIO.accessM(_.get.insert(user))
  }

  // Composition
  // Horizontal
  // ZLayer[In1, E1, Out1] ++ ZLayer[In2, E2, Out2] => ZLayer[In1 with In2, super(E1, E2), Out1 with Out2]

  val me = User("Joox", "thejooxman@gmail.com")
  val message = "Good morning Sunshine"

  val userBackendLayer = UserEmailer.live ++ UserDb.live
  UserEmailer.notify(me, message)
    .provideLayer(userBackendLayer)
    .exitCode

  // Vertical
  // Create a class to combine them with ZLayer.fromServices

  object UserSubscription {
    class Service(notifier: UserEmailer.Service, userDb: UserDb.Service) {
      def subscribe(user: User): Task[User] = {
        for {
          _ <- userDb.insert(user)
          _ <- notifier.notify(user, s"Welcome to my app ${user.name}")
        } yield user
      }
    }
    type UserSubscriptionEnv = Has[Service]

    val live = ZLayer.fromServices[UserEmailer.Service, UserDb.Service, Service]{(userEmailer, userDb) =>
      new Service(userEmailer, userDb)
    }

    def subscribe(user: User): ZIO[UserSubscriptionEnv, Throwable, User] =
      ZIO.accessM(_.get.subscribe(user))
  }

  val userSubscriptionLayer: ULayer[Has[UserSubscription.Service]] = userBackendLayer >>> UserSubscription.live
  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] = {


    UserEmailer
      .notify(me, message)
      .provideLayer(UserEmailer.live)
      .exitCode

    UserDb.insert(me)
      .provideLayer(UserDb.live)
      .exitCode

    UserSubscription
      .subscribe(me)
      .provideLayer(userSubscriptionLayer)
      .exitCode

  }
}
