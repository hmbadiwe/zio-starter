package com.example.zio.http

import pureconfig._
import pureconfig.generic.auto._
import zio.{Has, Layer, Task, ZIO, ZLayer}
import com.example.zio.Implicits._

object ApiConfiguration {
  trait Service {
    val load: Task[ApiConfig]
  }
  type ApiConfigurationEnv = Has[Service]

  def loadApiConfig: ZIO[ApiConfigurationEnv, Throwable, ApiConfig] = ZIO.accessM(_.get.load)

  val live: Layer[Throwable, ApiConfigurationEnv] = ZLayer.succeed(
    new ApiConfiguration.Service {
      override val load: Task[ApiConfig] = ConfigSource.default.at("api").load[ApiConfig].toTask
    }
  )
}
