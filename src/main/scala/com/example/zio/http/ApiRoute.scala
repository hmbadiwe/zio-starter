package com.example.zio.http

import com.example.zio.db.GamePersistence.GamePersistenceEnv
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe._
import zio._
import zio.interop.catz._
import io.circe.generic.auto._
import com.example.zio.db.GamePersistence._
import com.example.zio.domain.Game
import io.circe.{Decoder, Encoder}

class ApiRoute[R <: GamePersistenceEnv]{
  type GameTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[GameTask, A] = jsonOf[GameTask, A]
  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[GameTask, A] =
    jsonEncoderOf[GameTask, A]

  val dsl: Http4sDsl[GameTask] = Http4sDsl[GameTask]
  import dsl._

  def route: HttpRoutes[GameTask] =
    HttpRoutes.of[GameTask] {
      case GET -> Root => retrieveAllGames.foldM(th => NotFound(th.getMessage), Ok(_))
      case GET -> Root / UUIDVar(id) => retrieveGame(id).foldM(th => NotFound(th.getMessage), Ok(_))
    }
}

object ApiRoute {
  def apply[R <: GamePersistenceEnv]() = new ApiRoute[R]
}
