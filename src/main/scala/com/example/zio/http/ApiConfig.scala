package com.example.zio.http

case class ApiConfig(endpoint: String, port: Int)
