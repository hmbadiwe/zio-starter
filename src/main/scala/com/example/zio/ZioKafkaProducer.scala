package com.example.zio

import com.example.zio.ZioKafkaConsumer.gameSerde
import com.example.zio.domain.{Game, GamePlayer}
import org.apache.kafka.clients.producer.{ProducerRecord, RecordMetadata}
import zio.kafka.producer.{Producer, ProducerSettings}
import zio.kafka.serde.Serde
import zio.{Chunk, ExitCode, Has, RIO, URIO, ZIO, ZLayer, blocking}

import java.time.LocalDateTime
import java.util.UUID
import scala.util.Random

object ZioKafkaProducer extends zio.App {
  val countries = List("NED", "FRA", "GER", "ITA", "POR", "SUI", "SWE", "SPA", "BEL", "ENG")

  def countriesPairing: List[Game] = {
    val shuffledCountries = Random.shuffle(countries)
    shuffledCountries.zip(shuffledCountries.drop(1)).map{ case(away, home) =>
      val awayScore = Random.nextInt(5)
      val homeScore = Random.nextInt(5)
      Game(UUID.randomUUID(),
        LocalDateTime.now().minusMinutes(Random.nextLong(1000)),
        GamePlayer(away, awayScore),
        GamePlayer(home, homeScore))
    }
  }
  val producerSettings = ProducerSettings(List("localhost:9092"))
  val producerResource = Producer.make(producerSettings)

  val producer: ZLayer[blocking.Blocking, Throwable, Has[Producer]] = ZLayer.fromManaged(producerResource)

  val finalScore = Game(
    UUID.randomUUID(),
    LocalDateTime.now(),
    away = GamePlayer("NED", 0),
    home = GamePlayer("SUI", 0)
  )
  val record = new ProducerRecord[String, Game](ZioKafkaConsumer.topicName, "update-4", finalScore)
  val producerRecords = countriesPairing.map{ soccerMatch =>
    new ProducerRecord[UUID, Game](ZioKafkaConsumer.topicName, UUID.randomUUID(), soccerMatch)
  }
  val producerEffect = Producer.produce(record, Serde.string, ZioKafkaConsumer.gameSerde)

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] = {
    Producer
      .produceChunk(Chunk(producerRecords :_*), Serde.uuid, ZioKafkaConsumer.gameSerde)
      .provideSomeLayer(producer)
      .exitCode
  }
}
