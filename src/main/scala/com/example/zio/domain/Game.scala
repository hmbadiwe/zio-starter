package com.example.zio.domain

import doobie.util.{Get, Read}
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

import java.time.LocalDateTime
import java.util.UUID
import doobie.postgres.implicits._

case class Game(id: UUID, datePlayed: LocalDateTime, away: GamePlayer, home: GamePlayer){
  def score = s"${away.name} ${away.score} - ${home.score} ${home.name}"
}

object Game {
  implicit val encoder: JsonEncoder[Game] = DeriveJsonEncoder.gen[Game]
  implicit val decoder: JsonDecoder[Game] = DeriveJsonDecoder.gen[Game]

  implicit val readInstance: Read[Game] = Read[(UUID, LocalDateTime, String, Int, String, Int)].map{
    case(id, datePlayed, awayName, awayScore, homeName, homeScore) => Game(
      id,
      datePlayed,
      GamePlayer(awayName, awayScore),
      GamePlayer(homeName, homeScore))
  }
}