package com.example.zio.domain

import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class GamePlayer(name: String, score: Int)

object GamePlayer {
  implicit val encoder: JsonEncoder[GamePlayer] = DeriveJsonEncoder.gen[GamePlayer]
  implicit val decoder: JsonDecoder[GamePlayer] = DeriveJsonDecoder.gen[GamePlayer]
}
