package com.example.zio.db

import com.example.zio.db.GamePersistenceService.DbTransactorEnv
import com.example.zio.db.Persistence.PersistenceEnv
import com.example.zio.domain.Game
import doobie.util.transactor.Transactor
import zio.{RIO, Task, ZIO, ZLayer, blocking}
import doobie.implicits._
import zio.interop.catz._

import java.util.UUID

object GamePersistence {
  type GamePersistenceEnv = PersistenceEnv[Game]

  def createGame(g: Game): RIO[GamePersistenceEnv, Int] = RIO.accessM(_.get.create(g))
  def retrieveGame(id: UUID): RIO[GamePersistenceEnv, Game] = RIO.accessM(_.get.get(id))
  def retrieveAllGames: RIO[GamePersistenceEnv, Seq[Game]] = RIO.accessM(_.get.getAll)

  def createGamePlus(g: Game): ZIO[DbTransactorEnv, Throwable, Unit] =
    for {
      txn <- ZIO.service[Transactor[Task]]
      res <- GamePersistenceService.Sql.insert(g).run.transact(txn)
    } yield res

  val live: ZLayer[DbTransactorEnv, Throwable, GamePersistenceEnv] =
    ZLayer.fromService(GamePersistenceService(_))
}
