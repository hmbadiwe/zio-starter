package com.example.zio.db

import cats.effect.kernel.{Async, Resource}
import com.example.zio.db.DbConfiguration.{DbConfigurationEnv, loadDbConfig}
import com.example.zio.db.GamePersistence.GamePersistenceEnv
import com.example.zio.domain.{Game, GamePlayer}
import doobie.Query0
import doobie.h2.H2Transactor
import doobie.implicits._
import doobie.util.Read
import org.h2.jdbcx.JdbcConnectionPool
import org.postgresql.ds.{PGConnectionPoolDataSource, PGSimpleDataSource}

import java.time.LocalDateTime
import javax.sql.DataSource

//import doobie.postgres._
import doobie.postgres.implicits._
import zio.interop.catz._
import doobie.util.transactor.Transactor
import doobie.util.update.Update0
import zio._
import zio.interop.catz.implicits.rts
import zio.interop.catz._
import doobie.postgres.implicits._

import java.util.UUID
import scala.concurrent.ExecutionContext

object GamePersistenceService {
  type DbTransactorEnv = Has[Transactor[Task]]
  type GamePersistenceEnv = Has[GamePersistenceService]

  case class GameFlat(id: UUID, datePlayed: LocalDateTime, awayName: String, awayScore: Int, homeName: String, homeScore: Int){
    def toGame: Game = Game(id, datePlayed, GamePlayer(awayName, awayScore), GamePlayer(homeName, homeScore))
  }
  implicit val readInstance: Read[GameFlat] = Read[(UUID, LocalDateTime, String, Int, String, Int)].map{
    case (id, datePlayed, awayName, awayScore, homeName, homeScore) => GameFlat(id, datePlayed, awayName, awayScore, homeName, homeScore)
  }
  def apply(txn: Transactor[Task]): GamePersistenceService = new GamePersistenceService(txn)


  object Sql {
    def get(id: UUID): Query0[Game] =
      sql"""
        SELECT g.id,
               g.date_played,
               away.name as awayName,
               away.score AS awayScore,
               home.name AS homeName,
               home.score AS homeScore
        FROM game g
        INNER JOIN game_player home
        ON home.game_id = g.id
        INNER JOIN game_player away
        ON away.game_id = home.game_id
        AND away.is_home = false
        AND home.is_home = true
        AND g.id = ${id}
        """.query[Game]

    def getAll: Query0[Game] =
      sql"""
           SELECT g.id,
                  g.date_played AS datePlayed,
                  a.name        AS awayName,
                  a.score       AS awayScore,
                  h.name        AS homeName,
                  h.score       AS homeScore
            FROM game g
                INNER JOIN game_player a on g.away_id = a.id
                INNER JOIN game_player h on g.home_id = h.id
                WHERE a.is_home = false
                AND   h.is_home = true

           """.query[GameFlat].map(gfs => gfs.toGame)

    def insert(g: Game): Update0 =
      sql"""
        WITH insert_b AS (
           INSERT INTO game_player(name, score, game_id, is_home)
           values
           (${g.away.name}, ${g.away.score}, ${g.id}, false),
           (${g.home.name}, ${g.home.score}, ${g.id}, true)
           RETURNING *
        )
        insert into game(id, date_played, away_id, home_id)
        select
           ${g.id},
           ${g.datePlayed},
           a.id,
           h.id
           from insert_b h inner join
           insert_b a on a.game_id = h.game_id
           where a.is_home = false
           and h.is_home  = true
           """.update
  }

  def createPgDataSource(dbConfig: DbConfig): DataSource = {
    val ds = new PGSimpleDataSource
    ds.setUser(dbConfig.user)
    ds.setPassword(dbConfig.password)
    ds.setURL(dbConfig.url)
    ds
  }
  def mkTransactor(dbConfig: DbConfig, connectEc: ExecutionContext): TaskManaged[Transactor[Task]] = {
    val alloc = Async[Task].delay(createPgDataSource(dbConfig))
    val free  = (_: DataSource) => Task.succeed(())
    Resource.make(alloc)(free).map(Transactor.fromDataSource[Task](_, connectEc)).toManagedZIO
  }

  val transactorLive: ZLayer[DbConfigurationEnv with blocking.Blocking, Throwable, DbTransactorEnv] = ZLayer.fromManaged(
    for {
      dbConfig <- loadDbConfig.toManaged_
      blockingEc <- zio.blocking.blocking {
        ZIO.descriptor.map(_.executor.asEC)
      }.toManaged_
      transactor <- mkTransactor(dbConfig, blockingEc)
    } yield transactor
  )


}

class GamePersistenceService(txn: Transactor[Task]) extends Persistence.Service[Game]{
  import GamePersistenceService.Sql

  override def get(id: UUID): Task[Game] =
    Sql
      .get(id)
      .option
      .transact(txn)
      .foldM(th => Task.fail(th),
      matchOpt => Task.require(new RuntimeException("User not found"))(Task.succeed(matchOpt)))

  override def create(m: Game): Task[Int] =
    Sql
      .insert(m)
      .run
      .transact(txn)
      .foldM(err => Task.fail(err), queryInt => Task.succeed(queryInt))

  override def getAll: Task[Seq[Game]] =
    Sql
      .getAll
      .to[Seq]
      .transact(txn)
      .foldM(err => Task.fail(err), games => Task.succeed(games))
}
