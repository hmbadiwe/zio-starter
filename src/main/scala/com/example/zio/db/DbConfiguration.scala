package com.example.zio.db
import pureconfig._

import pureconfig.generic.auto._
import zio.{Has, Task, ULayer, ZIO, ZLayer}
import com.example.zio.Implicits._

object DbConfiguration {
  trait Service {
    val load: Task[DbConfig]
  }
  type DbConfigurationEnv = Has[Service]

  def loadDbConfig: ZIO[DbConfigurationEnv, Throwable, DbConfig] = ZIO.accessM(_.get.load)

  val live: ULayer[DbConfigurationEnv] = ZLayer.succeed(
    new DbConfiguration.Service {
      override val load: Task[DbConfig] = ConfigSource.default.at("db-config").load[DbConfig].toTask
    }
  )
}
