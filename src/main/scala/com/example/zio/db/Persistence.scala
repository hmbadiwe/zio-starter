package com.example.zio.db

import com.example.zio.domain.Game
import zio.{Has, RIO, Task}

import java.util.UUID

object Persistence {
  trait Service[A] {
    def get(id: UUID): Task[A]
    def create(m: A): Task[Int]
    def getAll: Task[Seq[Game]]
  }
  type PersistenceEnv[A] = Has[Service[A]]

}
