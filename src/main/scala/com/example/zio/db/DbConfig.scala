package com.example.zio.db

case class DbConfig(url: String, user: String, password: String)