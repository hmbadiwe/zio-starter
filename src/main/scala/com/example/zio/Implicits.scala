package com.example.zio

import zio.Task
import pureconfig.ConfigReader
import pureconfig.error.ConfigReaderFailures

object Implicits {
  implicit class RichConfigFailures(c: ConfigReaderFailures){
    def toThrowable: Throwable = {
      new RuntimeException(c.prettyPrint())
    }
  }

  implicit class RichConfigResult[T](configReaderResult: ConfigReader.Result[T]){
    def toTask: Task[T] = {
      Task.fromEither(configReaderResult.left.map(_.toThrowable))
    }
  }
}
